from threading import local
from flask import Flask, request, jsonify
import json

from flask_cors import CORS, cross_origin

import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor
from sklearn import preprocessing 
import datetime
from datetime import date
# from datetime import datetime

import pickle
import os
from utils import getIndexes, get_season, get_numdaysholidayevents
# from retail_data import holiday_data_csv, events_data_csv
#, weather_data_csv
from scheduler import fetch_csv

path = r"/home/newftpuser/ftp/upload/retail_pov"

dir_list = os.listdir(path)
retail_files = fetch_csv()

# if retail_files[1] in dir_list:
#     weather_file_path=os.listdir(r"/home/newftpuser/ftp/upload/retail_pov/" + str(retail_files[1]))
#     retail_df = pd.read_csv(weather_file_path)

if retail_files[3] in dir_list:
    publicholidays_file_path=(r"/home/newftpuser/ftp/upload/retail_pov/" + str(retail_files[3]))
    holiday_df = pd.read_csv(publicholidays_file_path)

if retail_files[2] in dir_list:
    events_file_path=(r"/home/newftpuser/ftp/upload/retail_pov/" + str(retail_files[2]))
    events_csv = pd.read_csv(events_file_path)    

demand_df = pd.read_csv("demand_df.csv")

orderhistory_model = pickle.load(open('orderhistory_model.pkl','rb'))

# holiday_df = pd.read_csv(holiday_data_csv())
# holiday_df = pd.read_csv("publicholidays_21-03-2022-17_00.csv")

# events_csv = pd.read_csv(events_data_csv(),on_bad_lines='skip')
# events_csv = pd.read_csv("localevents_21-03-2022-17_00.csv",on_bad_lines='skip')

Holiday_1_df = pd.read_csv("Holiday_1_df.csv")

holiday_model = pickle.load(open("holiday_model.pkl","rb"))

localweather_data = pd.read_csv("localweatherdata.csv")

localweather_model = pickle.load(open("localweather_model.pkl","rb"))

weatherprediction_model = pickle.load(open("rf_bestparams_weather_model.pkl","rb"))

localweatherapi_model = pickle.load(open("localweather_api_model.pkl","rb"))

merged_csv = pd.read_csv("merged_csv.csv", encoding="utf8",)

merged_model = pickle.load(open("merged_model.pkl","rb"))

f = open('le.obj', 'rb')
le_label = pickle.load(f)
f.close()

f_merged = open('merged_le.obj','rb')
merged_label = pickle.load(f_merged)
f_merged.close()

app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

#Common Features
product_id = demand_df["product_id"].tolist() 
base_price = demand_df["base_price$"].tolist()
current_selling_price = demand_df["current_selling_price$"].tolist()
current_profit_margin = demand_df["current_profit_margin$"].tolist()
Units_Sold = demand_df["Units_Sold"].tolist()

current_profit_margin_percent = ((demand_df["current_selling_price$"]-demand_df["base_price$"])/(demand_df["current_selling_price$"])*100)
   
productidlist = pd.Series(["Product Id" for x in range(len(product_id))])
currentsellingpricelist = pd.Series(["Current Selling Price" for x in range(len(current_selling_price))])
currentprofitlist = pd.Series(["Current Profit Margin" for x in range(len(current_profit_margin_percent))])

current_time = datetime.datetime.now() 
        
current_year = (current_time.year) 
        
current_month = (current_time.month) 
        
current_day = (current_time.day)

print ("The attributes of current date() are : ",current_year,current_month,current_day) 

@app.route("/")
def main():
    return jsonify({"Message" : "Active"})

@app.route('/orderhistorypriceprediciton', methods=["GET","POST"])
def orderhistorypriceprediciton():
     # 'product_id', 'base_price', 'predicted_price', 'current_profit_margin','Units_Sold'
    
    input = pd.DataFrame(list(zip(product_id,base_price,current_selling_price,current_profit_margin,Units_Sold)),columns=["product_id","base_price$","current_selling_price$",
                                                                      "current_profit_margin$","Units_Sold"])
    
    # print (input)

    predictedprice = orderhistory_model.predict(input.iloc[:,1:])
    
    predictedpricelist = pd.Series(["Predicted Price" for x in range(len(predictedprice))])

    predicted_profit_margin_percent = ((predictedprice - demand_df["base_price$"])/predictedprice*100)
    predictedprofitlist = pd.Series(["Predicted Profit" for x in range(len(predicted_profit_margin_percent))])


    prediction_list = list(zip(tuple(zip(productidlist,tuple(product_id))), 
                              tuple(zip(currentsellingpricelist,list(np.around(current_selling_price,2)))),
                              tuple(zip(currentprofitlist,list(np.around(current_profit_margin_percent,2)))),
                              tuple(zip(predictedpricelist,list(np.around(predictedprice,2)))),
                              tuple(zip(predictedprofitlist,list(np.around(predicted_profit_margin_percent,2))))
                                    )
                         )
    final_prediction_list = []
    
    for list1 in prediction_list:
        prediction_dict = dict(list1)
        final_prediction_list.append(prediction_dict)
    return jsonify({"Predicted Price" : final_prediction_list})


@app.route('/holidayeventspriceprediciton', methods=["POST"])
def holidayeventspriceprediciton():

    # def get_list(headers):
    #     values = [request.form.getlist(h) for h in headers]
    #     items = [{} for i in range(len(values[0]))]
    #     for x,i in enumerate(values):
    #         for _x,_i in enumerate(i):
    #             items[_x][headers[x]] = _i
    #     return items

    # @app.route("/save", methods=["POST"])
    # def save_path():
    #     return jsonify(get_list(('label', 'price', 'quantity')))
    
    data = request.get_json()
    event_type = data["event_type"] 
    print(event_type)
    description = data["description"]
    print(description)
    input_date = data["input_date"]
    print(input_date)
    input_date = pd.to_datetime(input_date)
    print(input_date)
    input_date = datetime.datetime.strptime(str(input_date.date()),"%Y-%m-%d").strftime("%d-%m-%Y")

    print(input_date)
 
    if event_type == "holiday" : 
        description = data["description"]
        index_holiday = getIndexes(holiday_df, description)
        print(index_holiday)

        holiday_date = list(holiday_df["Date Of Holiday"].iloc[index_holiday])
        holiday_date = pd.to_datetime(holiday_date[0])
        print(holiday_date)
        holiday_date = datetime.datetime.strptime(str(holiday_date.date()),"%Y-%m-%d").strftime("%d-%m-%Y")        
        print(holiday_date)
        if holiday_date != input_date:
            return {"Message" : "User entered date is not matching with the holdiday or event date"}
        holiday_day = int(holiday_date.split("-")[0])
        holiday_month = int(holiday_date.split("-")[1])
        holiday_year = int(holiday_date.split("-")[2])
        days = (date(2022,int(holiday_month),int(holiday_day)) - date(2022,int(current_month),int(current_day))).days
        if days <= 0:
            days = 0
        else:
            pass
        print(days)

    elif event_type == "local event" :
        description = data["description"]
        index_events = getIndexes(events_csv, description)
        print(index_events)
        event_date = list(events_csv["Start Date"].iloc[index_events])
        print(event_date)
        event_date = pd.to_datetime(event_date[0])
        print(event_date)
        event_date = datetime.datetime.strptime(str(event_date.date()),"%Y-%m-%d").strftime("%d-%m-%Y")        
        print(event_date)

        if event_date != input_date:
            return {"Message" : "User entered date is not matching with the holdiday or event date"}
        events_day = int(event_date.split("-")[0])
        print(events_day)
        events_month = int(event_date.split("-")[1])
        print(events_month)
        events_year = int(event_date.split("-")[2])
        days = (date(2022,int(events_month),int(events_day)) - date(2022,int(current_month),int(current_day))).days
        if days <= 0:
            days = 0
        else:
            pass
    
    num_ofdays = pd.Series([days for x in range(len(Holiday_1_df.index))])

    input = pd.DataFrame(list(zip(product_id,base_price,current_selling_price,current_profit_margin,Units_Sold,num_ofdays)),columns=["product_id",
                                    "base_price","current_selling_price","current_profit_margin","Units_Sold","num_ofdays"])
    
    # print (input)

    holidaypredictedprice = holiday_model.predict(input.iloc[:,1:])
    holidaypredicted_profit_margin = ((holidaypredictedprice - base_price)/holidaypredictedprice*100)
    

    predictedprice = holiday_model.predict(input.iloc[:,1:])
    predicted_profit_margin_percent = ((predictedprice - demand_df["base_price$"])/predictedprice*100)
    
    predictedpricelist = pd.Series(["Predicted Price" for x in range(len(predictedprice))])
    predictedprofitlist = pd.Series(["Predicted Profit" for x in range(len(predicted_profit_margin_percent))])

    prediction_list = list(zip(tuple(zip(productidlist,tuple(product_id))), 
                              tuple(zip(currentsellingpricelist,list(np.around(current_selling_price)))),
                              tuple(zip(currentprofitlist,list(np.around(current_profit_margin_percent)))),
                              tuple(zip(predictedpricelist,list(np.around(predictedprice)))),
                              tuple(zip(predictedprofitlist,list(np.around(predicted_profit_margin_percent))))
                                    )
                         )
    final_prediction_list = []
    
    for list1 in prediction_list:
        prediction_dict = dict(list1)
        final_prediction_list.append(prediction_dict)
    return jsonify({"Predicted Price" : final_prediction_list})


@app.route('/localweatherprediction', methods=["POST"])
def localweatherprediction():
     # 'product_id', 'base_price', 'predicted_price', 'current_profit_margin','Units_Sold'
    
    
    season = pd.Series([get_season(date(int(current_year),int(current_month),int(current_day))) for x in range(len(localweather_data.index))])
    season = le_label.transform(season).tolist()
    # season = localweather_data["season"].replace({'spring': 0, 'summer': 1, 'winter': 2, 'autumn': 3},inplace=False).tolist()

    # print(season)
     
    input = pd.DataFrame(list(zip(season,product_id,base_price,current_selling_price,current_profit_margin,Units_Sold)),columns=["season","product_id",
                                  "base_price$","current_selling_price$","current_profit_margin$",
                                  "Units_Sold"])
    

    predictedprice = localweather_model.predict(input.iloc[:,[0,2,3,4,5]])
    print(predictedprice)
    
    predictedpricelist = pd.Series(["Predicted Price" for x in range(len(predictedprice))])

    predicted_profit_margin_percent = ((pd.Series(predictedprice) - demand_df["base_price$"])/pd.Series(predictedprice)*100)
    print(demand_df["base_price$"])
    predictedprofitlist = pd.Series(["Predicted Profit" for x in range(len(predicted_profit_margin_percent))])


    prediction_list = list(zip(tuple(zip(productidlist,tuple(product_id))), 
                              tuple(zip(currentsellingpricelist,list(np.around(current_selling_price,2)))),
                              tuple(zip(currentprofitlist,list(np.around(current_profit_margin_percent,2)))),
                              tuple(zip(predictedpricelist,list(np.around(predictedprice,2)))),
                              tuple(zip(predictedprofitlist,list(np.around(predicted_profit_margin_percent,2))))
                              )
                         )
    final_prediction_list = []
    
    for list1 in prediction_list:
        prediction_dict = dict(list1)
        final_prediction_list.append(prediction_dict)
    return jsonify({"Predicted Price" : final_prediction_list})

@app.route('/localweatherapiprediction', methods=["POST"])
def localweatherapiprediction():
    data = request.get_json()
    temperature = data["temperature"] 
    print(temperature)
    pressure = data["pressure"] 
    print(pressure)
    wind_speed = data["wind_speed"] 
    print(wind_speed)
    
    input_weather =  pd.DataFrame({"temperature" : [temperature], "pressure" : [pressure],
                         "wind_speed" : [wind_speed]})
    print(input_weather)


    predictedweather = weatherprediction_model.predict(input_weather.iloc[:,:])
    # print(predictedweather)
    
    weatherprediction = pd.Series([predictedweather[0] for x in range(len(demand_df))])

    input = pd.DataFrame(list(zip(product_id,base_price,current_selling_price,current_profit_margin,Units_Sold,weatherprediction)),columns=["product_id",
                                  "base_price$","current_selling_price$","current_profit_margin$","Units_Sold","weather"])
    

    predictedprice = localweatherapi_model.predict(input.iloc[:,[1,2,3,4,5]])
    print(predictedprice)
    
    predictedpricelist = pd.Series(["Predicted Price" for x in range(len(predictedprice))])

    predicted_profit_margin_percent = ((pd.Series(predictedprice) - demand_df["base_price$"])/pd.Series(predictedprice)*100)
    print(demand_df["base_price$"])
    predictedprofitlist = pd.Series(["Predicted Profit" for x in range(len(predicted_profit_margin_percent))])


    prediction_list = list(zip(tuple(zip(productidlist,tuple(product_id))), 
                              tuple(zip(currentsellingpricelist,list(np.around(current_selling_price,2)))),
                              tuple(zip(currentprofitlist,list(np.around(current_profit_margin_percent,2)))),
                              tuple(zip(predictedpricelist,list(np.around(predictedprice,2)))),
                              tuple(zip(predictedprofitlist,list(np.around(predicted_profit_margin_percent,2))))
                              )
                         )
    final_prediction_list = []
    
    for list1 in prediction_list:
        prediction_dict = dict(list1)
        final_prediction_list.append(prediction_dict)
    return jsonify({"Predicted Price" : final_prediction_list})


@app.route('/mergedresponseprediction', methods=["POST"])
def mergedresponseprediction():

    category = data["category"] 
    # print(competitors_name)
    # product_id,base_price,competitors_selling_price,current_profit_margin,Units_Sold

    df = demand_df.loc[(demand_df["Level-1 Category"] == str(category))]

    data = request.get_json()
    print("********" , data)
    holiday = data["holiday"] 
    print(holiday)

    if holiday:
        input_holiday_date = data["input_holiday_date"]
        print(input_holiday_date)
        input_holiday_date = pd.to_datetime(input_holiday_date[0])
        print(input_holiday_date)
        input_holiday_date = datetime.datetime.strptime(str(input_holiday_date.date()),"%Y-%m-%d").strftime("%d-%m-%Y")
        print(input_holiday_date)
        index_holiday = getIndexes(holiday_df, holiday[0])
        print(index_holiday)
        holiday_date = list(holiday_df["Date Of Holiday"].iloc[index_holiday])
        print(holiday_date)
        holiday_date = pd.to_datetime(holiday_date[0])
        print(holiday_date)
        holiday_date = datetime.datetime.strptime(str(holiday_date.date()),"%Y-%m-%d").strftime("%d-%m-%Y")  
        print(holiday_date)

        if holiday_date != input_holiday_date:
            return {"Message" : "User entered holiday date is not matching with the holdiday or event date"}
        holiday_day = int(holiday_date.split("-")[0])
        holiday_month = int(holiday_date.split("-")[1])
        # holiday_year = int(holiday_date.split("-")[2])
        
        numofdays_holiday = get_numdaysholidayevents(current_month,current_day,holiday_month,holiday_day)
    
        
    else:
        numofdays_holiday = -1

    print(numofdays_holiday)

    event = data["event"] 
    print(event)
    
    if event:
        input_event_date = data["input_event_date"]
        print(input_event_date)
        input_event_date = pd.to_datetime(input_event_date[0])
        print(input_event_date)
        input_event_date = datetime.datetime.strptime(str(input_event_date.date()),"%Y-%m-%d").strftime("%d-%m-%Y")
        print(input_event_date)
        index_events = getIndexes(events_csv, event[0])
        print(index_events)
        event_date = list(events_csv["Start Date"].iloc[index_events])
        print(event_date)
        event_date = pd.to_datetime(event_date[0])
        print(event_date)
        event_date = datetime.datetime.strptime(str(event_date.date()),"%Y-%m-%d").strftime("%d-%m-%Y")        
        print(event_date)
        # event_date = datetime.datetime.strptime(str(event_date[0]), "%Y-%m-%d").strftime("%d-%m-%Y")
        if event_date != input_event_date:
            return {"Message" : "User entered event date is not matching with the holdiday or event date"}
        events_day = int(event_date.split("-")[0])
        print(events_day)
        events_month = int(event_date.split("-")[1])
        print(events_month)
        events_year = int(event_date.split("-")[2])
        numofdays_events = get_numdaysholidayevents(current_month,current_day,events_month,events_day)


    else:
        numofdays_events = -1

    print(numofdays_events)

    num_ofdays_holiday = pd.Series([numofdays_holiday for x in range(len(merged_csv))])
    num_ofdays_events = pd.Series([numofdays_events for x in range(len(merged_csv))])

    temperature = data["temperature"] 
    print(temperature)
    pressure = data["pressure"] 
    print(pressure)
    wind_speed = data["wind_speed"] 
    print(wind_speed)

    if temperature or pressure or wind_speed:
        input_weather = input = pd.DataFrame({"temperature" : [temperature[0]], "pressure" : [pressure[0]],
                            "wind_speed" : [wind_speed[0]]})
        print(input_weather)
        predictedweather = weatherprediction_model.predict(input_weather.iloc[:,:])
        # print(predictedweather)

    else:
        predictedweather = [-1]
   
    weatherprediction = pd.Series([predictedweather[0] for x in range(len(demand_df))])

    input = pd.DataFrame(list(zip(product_id,base_price,current_selling_price,current_profit_margin,
                         Units_Sold,num_ofdays_holiday,num_ofdays_events,weatherprediction)),columns=["product_id",
                         "base_price","current_selling_price","current_profit_margin","Units_Sold",
                         "num_ofdays_holiday","num_ofdays_events","weather"])
    
    # print (input)

    predictedprice = merged_model.predict(input.iloc[:,1:])
    
    predictedpricelist = pd.Series(["Predicted Price" for x in range(len(predictedprice))])

    predicted_profit_margin_percent = ((predictedprice - demand_df["base_price$"])/predictedprice*100)
    predictedprofitlist = pd.Series(["Predicted Profit" for x in range(len(predicted_profit_margin_percent))])


    prediction_list = list(zip(tuple(zip(productidlist,tuple(product_id))), 
                              tuple(zip(currentsellingpricelist,list(np.around(current_selling_price,2)))),
                              tuple(zip(currentprofitlist,list(np.around(current_profit_margin_percent,2)))),
                              tuple(zip(predictedpricelist,list(np.around(predictedprice,2)))),
                              tuple(zip(predictedprofitlist,list(np.around(predicted_profit_margin_percent,2))))
                                    )
                         )
    final_prediction_list = []
    
    for list1 in prediction_list:
        prediction_dict = dict(list1)
        final_prediction_list.append(prediction_dict)
    return jsonify({"Predicted Price" : final_prediction_list})

@app.route('/competitorsprediction', methods=["POST"])
def competitorsprediction():
    data = request.get_json()
    print("******",data)
    competitors_data = pd.read_csv("competitors_df.csv")
    competitors_model = pickle.load(open('competitors_model.pkl','rb'))

    # competitors_data.shape
    # competitors_data.columns
    # competitors_data["Competitor"].unique()
    competitors_name_list = data["competitors_name"] 
    # print(competitors_name)
    # product_id,base_price,competitors_selling_price,current_profit_margin,Units_Sold
    df_list = []
    for competitors_name in competitors_name_list:
        df = competitors_data.loc[(competitors_data["Competitor"] == str(competitors_name))]
        df_list.append(df)
    
    df = pd.concat(df_list)
    # print(df.head)
    # product_id,base_price,competitors_selling_price,current_profit_margin,Units_Sold

    # product_id = df["Product Code"].tolist()
    base_price = df["Base Price"].tolist()
    competitors_selling_price = df["Competitors Price"].tolist()
    current_selling_price = df["Current Selling Price"].tolist()
    current_profit_margin = df["Current Profit Margin"].tolist()
    Units_Sold = df["Sold Quantity"].tolist()
    input = pd.DataFrame(list(zip(base_price,competitors_selling_price,current_selling_price,current_profit_margin,Units_Sold)),columns=["base_price$","competitors_selling_price$","current_selling_price$","current_profit_margin$","Units_Sold"])

    # print(input)


    predictedprice = competitors_model.predict(input)
    predictedprice
    
    predictedpricelist = pd.Series(["Predicted Price" for x in range(len(predictedprice))])

    predicted_profit_margin_percent = ((predictedprice - df["Base Price"])/predictedprice*100)
    predictedprofitlist = pd.Series(["Predicted Profit" for x in range(len(predicted_profit_margin_percent))])
    competitorspricelist = pd.Series(["Competitors Price" for x in range(len(predicted_profit_margin_percent))])
    currentpricelist = pd.Series(["Current Selling Price" for x in range(len(predicted_profit_margin_percent))])


    prediction_list = list(zip(tuple(zip(productidlist,tuple(product_id))), 
                              tuple(zip(competitorspricelist,list(np.around(competitors_selling_price,2)))),
                              tuple(zip(currentpricelist,list(np.around(current_selling_price,2)))),
                              tuple(zip(currentprofitlist,list(np.around(current_profit_margin_percent,2)))),
                              tuple(zip(predictedpricelist,list(np.around(predictedprice,2)))),
                              tuple(zip(predictedprofitlist,list(np.around(predicted_profit_margin_percent,2))))
                                    )
                         )
    final_prediction_list = []
    
    for list1 in prediction_list:
        prediction_dict = dict(list1)
        final_prediction_list.append(prediction_dict)
    return jsonify({"Predicted Price" : final_prediction_list})
    
     
if __name__ == "__main__":
    app.run(debug=True,port=5007, host="0.0.0.0")
#