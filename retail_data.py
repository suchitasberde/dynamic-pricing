import pandas as pd
import time
import datetime
from scheduler import fetch_csv
# from scheduler.py import retail_pov_csv

# files = ['retail-pov-store-data_17-03-2022 17_00.csv', 'localevents_17-03-2022 17_00.csv', 
#         'publicholidays_17-03-2022 17_00.csv', 'weather_17-03-2022 17_00.csv']

# ['retail-pov-store-data_22-03-2022-17_00.csv', 'weather_22-03-2022-17_00.csv',
#  'publicholidays_22-03-2022-17_00.csv', 'localevents_22-03-2022-17_00.csv']
import os
# retail_path = r"/home/newftpuser/ftp/upload/retail_pov/retail-pov-store-data_22-03-2022-17_00.csv"
path = r"/home/newftpuser/ftp/upload/retail_pov"

dir_list = os.listdir(path)
retail_files = fetch_csv()
# print(retail_files)
# ['weather_12-04-2022-17_00.csv', 'retail-pov-store-data_12-04-2022-17_00.csv', 'localevents_12-04-2022-17_00.csv', 'publicholidays_12-04-2022-17_00.csv']

# if retail_files[1] in dir_list:
#     retail_pov_file_path=(r"/home/newftpuser/ftp/upload/retail_pov/" + str(retail_files[1]))
#     retail_df = pd.read_csv(retail_pov_file_path)

retail_df = pd.read_csv("Retail_updated_store_data_competitor.csv")

# retail_df = pd.read_csv("retail-pov-store-data_competitorsprice.csv")
training_cols = ["Product Code", "Base Price", "Current Selling Price", "Predicted Price", 
                 "Current Profit Margin","Predicted Profit Margin","Sold Quantity","Level-1 Category"]

training_data = retail_df[training_cols]

demand_df = pd.DataFrame()
demand_df["product_id"] = training_data["Product Code"]
demand_df["base_price$"] = training_data["Base Price"]
demand_df["current_selling_price$"] = training_data["Current Selling Price"]
demand_df["current_profit_margin$"] = training_data["Current Profit Margin"]
demand_df["Units_Sold"] = training_data["Sold Quantity"]
demand_df["predicted_price$"] = training_data["Predicted Price"]
demand_df["Level-1 Category"] = training_data["Level-1 Category"]


demand_df.to_csv("demand_df.csv")

Holiday_1_df = pd.DataFrame()
Holiday_1_df["product_id"] = demand_df["product_id"]
Holiday_1_df["base_price"] = demand_df["base_price$"]
Holiday_1_df["current_selling_price"] = demand_df["current_selling_price$"]
Holiday_1_df["predicted_price"] = demand_df["predicted_price$"]
Holiday_1_df["current_profit_margin"] = demand_df["current_profit_margin$"]
Holiday_1_df["Units_Sold"] = demand_df["Units_Sold"]
Holiday_1_df["Num_Holiday"] = retail_df["NoOf Days Left For Holidays"]

Holiday_1_df.to_csv("Holiday_1_df.csv")

def holiday_data_csv():
    return retail_files[2]

def events_data_csv():
    return retail_files[3]

# def weather_data_csv():
#     return retail_files[1]



