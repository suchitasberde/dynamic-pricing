import datetime
from datetime import date

def Convert(tup, di):
        di = dict(tup)
        return di

def getIndexes(dfObj, value):
    # ''' Get index positions of value in dataframe i.e. dfObj.'''
        listOfPos = list()
    # Get bool dataframe with True at positions where the given value exists
        result = dfObj.isin([value])
    # Get list of columns that contains the value
        seriesObj = result.any()
        columnNames = list(seriesObj[seriesObj == True].index)
    # Iterate over list of columns and fetch the rows indexes where value exists
        for col in columnNames:
            rows = list(result[col][result[col] == True].index)
            for row in rows:
                listOfPos.append((row))
    # Return a list of tuples indicating the positions of value in the dataframe
        return listOfPos

def get_season(now):
        # if isinstance(now, datetime):
        #     now = now.date()
    Y = 2022 # dummy leap year to allow input X-02-29 (leap day)
    seasons = [('winter', (date(Y,  1,  1),  date(Y,  3, 20))),
           ('spring', (date(Y,  3, 21),  date(Y,  6, 20))),
           ('summer', (date(Y,  6, 21),  date(Y,  9, 22))),
           ('autumn', (date(Y,  9, 23),  date(Y, 12, 20))),
           ('winter', (date(Y, 12, 21),  date(Y, 12, 31)))]
        
    now = now.replace(year=Y)
    return next(season for season, (start, end) in seasons
            if start <= now <= end)
 
def get_numdaysholidayevents(current_month,current_day,h_e_month,h_e_day):

    days = (date(2022,int(current_month),int(current_day)) - date(2022,int(h_e_month),int(h_e_day))).days
    if days <= 0:
        days = 0
    else:
        pass
    return days 